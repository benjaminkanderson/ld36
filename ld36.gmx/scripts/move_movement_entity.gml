///move_movement_entity()
/*
    This function updates the position of the movement entity
    according to its horizontal speed and vertical speed.
    This function should be called at the end of the STEP EVENT for each
    object you want using the movement functions
*/
hsp[0] = lengthdir_x(spd[0], dir[0]);
vsp[0] = lengthdir_y(spd[0], dir[0]);
var hspd = hsp[0]+hsp[1];
var vspd = vsp[0]+vsp[1];

// Horizontal check
if (place_meeting(x+hspd, y, collision_object)) {
    while (!place_meeting(x+sign(hspd), y, collision_object)) {
        x+=sign(hspd);
    }
    
    // Update the hspeed
    hspd = 0;
    hsp[0] = 0;
    hsp[1] = -(hsp[1])*bounce;
    
    // Stop bounce at low values
    if (abs(hsp[1]) < 1 && bounce) hsp[1] = 0;
}
x += hspd;

// Vertical collision check
if (place_meeting(x, y+vspd, collision_object)) {
    while (!place_meeting(x, y+sign(vspd), collision_object)) {
        y+=sign(vspd);
    }
    vspd = 0;
    vsp[0] = 0;
    vsp[1] = -vsp[1]*bounce;
    
    // Stop bounce at low values
    if (abs(vsp[1]) < 1 && bounce) vsp[1] = 0;
}
y += vspd;

// Update the spd and dir
spd[0] = point_distance(0, 0, hsp[0], vsp[0]);
dir[0] = point_direction(0, 0, hsp[0], vsp[0]);

// Friction
apply_movement_friction();
horizontal_move_input = false;
vertical_move_input = false;

