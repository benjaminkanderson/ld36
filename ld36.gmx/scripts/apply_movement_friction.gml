///apply_movement_friction()
// Knockback Friction
var dir = point_direction(0, 0, hsp[1], vsp[1]);
var hfric = abs(lengthdir_x(fric, dir));
var vfric = abs(lengthdir_y(fric, dir));
hsp[1] = approach(hsp[1], 0, hfric);
vsp[1] = approach(vsp[1], 0, vfric);


// Inpout Friction
if (!horizontal_move_input && !vertical_move_input) {
    spd[0] = approach(spd[0], 0, fric);
}
