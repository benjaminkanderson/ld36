///draw_shadow(yoffset, yscale);
var yoffset = argument0;
var yscale = argument1;

draw_sprite_ext(sprite_index, image_index, x, y+yoffset, image_xscale, yscale, image_angle, c_black, .3);
