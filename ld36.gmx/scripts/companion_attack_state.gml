///companion_attack_state
sprite_index = s_charmander_attack;
if (animation_end()) {
    if (instance_exists(o_enemy_parent)) {
        var enemy = instance_nearest(x, y, o_enemy_parent);
        var dir = point_direction(x, y, enemy.x, enemy.y)+random_range(-10, 10);
        state = companion_idle_state;
        image_xscale = true_sign(enemy.x-x);
        var fireball = instance_create(x, y-8, o_fireball);
        fireball.direction = dir;
        fireball.image_angle = dir;
        fireball.creator = id;
        alarm[0] = fireball.cooldown;
    }
}

