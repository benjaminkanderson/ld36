///input_move_to_spawn_point()
if (instance_exists(o_spawn_point)) {
    with (o_spawn_point) {
        if (index == other.index) {
            x = o_spawn_point.x;
            y = o_spawn_point.y;
        }
    }
}
