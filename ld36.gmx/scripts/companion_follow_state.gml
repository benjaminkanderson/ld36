///companion_follow_state()
if (instance_exists(o_enemy_parent)) {
    var enemy = instance_nearest(x, y, o_enemy_parent);
    if (distance_to_object(enemy) < 128) {
        state = companion_chase_state;
    }
}

// Follow the player
if (instance_exists(PLAYER)) {
    if (distance_to_object(PLAYER) > 16) {
        sprite_index = s_charmander_run;
        image_xscale = true_sign(PLAYER.x-x);
        var dir = point_direction(x, y, PLAYER.x, PLAYER.y);
        add_movement_direction_acceleration_maxspeed(dir, acc, max_spd);
    } else {
        sprite_index = s_charmander_idle;
    }
}
