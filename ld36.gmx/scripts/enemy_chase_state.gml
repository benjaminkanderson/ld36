///enemy_chase_state()
if (instance_exists(PLAYER)) {
    var dir = point_direction(x, y, PLAYER.x, PLAYER.y);
    
    if (distance_to_object(PLAYER) > 96) {
        add_movement_direction_acceleration_maxspeed(dir, acc, max_spd);
    } else if (alarm[0] <= 0) {
        var laser = instance_create(x, y-6, o_enemy_laser);
        laser.direction = dir+random_range(-10, 10);
        laser.image_angle = laser.direction;
        laser.creator = id;
        alarm[0] = laser.cooldown;
        add_movement_direction_acceleration_maxspeed(dir-180, laser.recoil, 99);
    }
    image_xscale = true_sign(PLAYER.x-x);
}
