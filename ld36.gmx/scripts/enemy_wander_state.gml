///enemy_wander_state()
var dir = point_direction(x, y, target[? "x"], target[? "y"]);
var dis = point_distance(x, y, target[? "x"], target[? "y"]);
if (dis > 32) {
    add_movement_direction_acceleration_maxspeed(dir, acc/2, max_spd);
} else {
    target[? "x"] = clamp(x+lengthdir_x(128, random(360)), 16, room_width-16);
    target[? "y"] = clamp(y+lengthdir_y(128, random(360)), 16, room_height-16);
}

// Chase state
if (instance_exists(PLAYER)) {
    if (point_distance(x, y, PLAYER.x, PLAYER.y) < 240) {
        state = enemy_chase_state;
    }
}

