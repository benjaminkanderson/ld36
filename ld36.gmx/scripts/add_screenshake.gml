///add_screenshake(amount, duration)
var amount = argument0;
var duration = argument1;

if (exists(o_camera)) {
    o_camera.shake = amount;
    o_camera.alarm[0] = duration;
}
