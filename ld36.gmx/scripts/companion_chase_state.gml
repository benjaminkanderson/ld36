///companion_chase_state()

if (instance_exists(o_enemy_parent)) {
    var enemy = instance_nearest(x, y, o_enemy_parent);
    if (distance_to_object(enemy) > 128) {
        state = companion_follow_state;
    } else {
        sprite_index = s_charmander_run;
        image_xscale = true_sign(enemy.x-x);
        var dir = point_direction(x, y, enemy.x, enemy.y);
        add_movement_direction_acceleration_maxspeed(dir, acc, max_spd);
        
        if (distance_to_object(enemy) <= 96) {
            state = companion_attack_state;
        }
    }
}
