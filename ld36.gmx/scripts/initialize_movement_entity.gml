///initialize_movement_entity(friction, bounce, collision_object)
/*
    This function is used to initialize a movement entity.
    You need to call this function in the CREATE EVENT of
    any object you would like using the movement functions.
*/

// We use two speeds to control input and knockback

// Input speeds
hsp[0] = 0;
vsp[0] = 0;
spd[0] = 0;
dir[0] = 0;

// Knockback speeds
hsp[1] = 0;
vsp[1] = 0;

fric = argument[0]; // Friction amount
bounce = argument[1]; // Bounce amount. (Only affects knockbac speed) 0 is no bounce, .5 is half velocity lost, 1 is no velocity lost.
collision_object = argument[2] // The object that will be used for collisions.

horizontal_move_input = false;
vertical_move_input = false;
