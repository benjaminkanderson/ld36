///read_input()
gamepad_set_axis_deadzone(index, .4);
left_xaxis = gamepad_axis_value(index, gp_axislh);
left_yaxis = gamepad_axis_value(index, gp_axislv);
abtn = gamepad_button_check_pressed(index, gp_face3);
xbtn =  gamepad_button_check_pressed(index, gp_face1);
bbtn = gamepad_button_check_pressed(index, gp_face2);
ybtn = gamepad_button_check_pressed(index, gp_face4);
right_xaxis = gamepad_axis_value(index, gp_axisrh);
left_yaxis = gamepad_axis_value(index, gp_axisrv);
trigger = gamepad_button_check(index, gp_shoulderrb);

