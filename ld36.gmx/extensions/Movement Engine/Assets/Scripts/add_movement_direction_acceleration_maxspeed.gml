///add_movement_direction_acceleration_maxspeed(direction, acceleration, maxspeed);
/*
    This function adds a direction and acceleration
    to a movement entity but it will not exceed the mas acceleration.
    This added acceleration will be relative to 
    the previous speed of that entity.
    
    It works very much like
    motion_add only with a maximum speed
*/

var dir = argument[0]; // Direction value
var acc = argument[1]; // Acceleration value
var maxspd = argument[2]; // The maximum speeed value

// Get the temp hspd
var hspd = lengthdir_x(spd[0], id.dir[0]);
var vspd = lengthdir_y(spd[0], id.dir[0]);

// Get the accelerations
var hacc = abs(lengthdir_x(acc, dir));
var vacc = abs(lengthdir_y(acc, dir));

// Test for input
horizontal_move_input = hacc != 0;
vertical_move_input = vacc != 0;

// Add the new speed to the previous speeds
hspd += lengthdir_x(hacc, dir);
vspd += lengthdir_y(vacc, dir);

// Update the speed and direction
spd[0] = point_distance(0, 0, hspd, vspd);
id.dir[0] = point_direction(0, 0, hspd, vspd);

// Clamp the speed
spd[0] = min(spd[0], maxspd);
